## Módulo 2

### React

- Aprender o básico do Framework.
- Fazer 10 escritas diferentes de Hello World.

### Metodologias Ágeis / Scrum

- Conhecer Metodologias Ágeis e para que servem.
- Entender o formato do Scrum (que é utilizado na ACCT).
- Conhecer as cerimônias que ocorrem no Scrum.

### NPM e Yarn

- Conhecer os gerenciadores de pacotes.
- Entender como funcionam.
- Fazer a instalação do Vtex CLI, que é o gerenciador de pacotes da Vtex.

### Vtex Overview

- Primeiros passos para conhecer a plataforma Vtex.

### APIs e Postman

- Entender como funcionam as requisições HTTP.
- Entender o que são APIs.
- Fazer requests de teste pelo aplicativo Postman.

### APIs Vtex

- Conhecer as APIs da Vtex.
- Fazer uma request em uma API pública da Vtex pelo Postman.
