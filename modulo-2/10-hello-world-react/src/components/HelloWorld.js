import React from 'react'

export function HelloWorld() {
  let hello = 'Hello World!'

  return (
    <>
      <div className="hello1">{hello}</div>
      <div className="hello2">{hello.toUpperCase()}</div>
      <div className="hello3">{hello.toLowerCase()}</div>
      <div className="hello4">{hello.repeat(2)}</div>
      <div className="hello5">{hello.replace('World!', 'World (Mundo)!')}</div>
      <div className="hello6">{hello.slice(0, 11)}</div>
      <div className="hello7">{hello.split(' ')}</div>
      <div className="hello8">{hello.split(' ').reverse()}</div>
      <div className="hello9">{hello.split('').reverse()}</div>
      <div className="hello10">
        <div>{hello[0]}</div>
        <div>{hello[1]}</div>
        <div>{hello[2]}</div>
        <div>{hello[3]}</div>
        <div>{hello[4]}</div>
        <br />
        <div>{hello[5]}</div>
        <div>{hello[6]}</div>
        <div>{hello[7]}</div>
        <div>{hello[8]}</div>
        <div>{hello[9]}</div>
        <div>{hello[10]}</div>
        <div>{hello[11]}</div>
      </div>
    </>
  )
}
