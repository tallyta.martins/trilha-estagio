import React from 'react'
import { HelloWorld } from './components/HelloWorld.js'
import './App.css'

function App() {
  return (
    <div>
      <HelloWorld className="helloWorld" />
    </div>
  )
}

export default App
