## Módulo 3

### Figma

- Entender o que é e para que serve o Figma.

### Onboarding Vtex

- Conhecer a plataforma admin da Vtex, onde é possível gerenciar a loja de forma prática.
- Saber como cadastra Categorias, Subcategorias e SKUs.
- Saber como funciona a regra de preços, para cadastras nos SKUS.
- Entender as formas de promoção que podem ser configuradas dentro da plataforma.
- Saber como funciona o fluxo de pedido e formas de pagamento.
- Aprender sobre Marketplace e entrega.
- Para particar: cadastrar um produto com SKUs diferentes, dentro de uma Categoria pré-definida.

### Vtex IO: Conceitos

- Conhecer a estrutura low code utilizada para desenvolvimento de e-commerce, onde, dentre outras coisas, possui diversos componentes já feitos para que possam ser utilizados por diversas lojas.
- O Vtex IO possui também, inúmeras APIS para serem consumidas dentro da fabricação da loja, diminuindo tempo de desenvolvimento e tornando o trabalho mais ágil.
- Através do Vtex CLI pé possível utilizar a interface de linha de comando para ter acesso aos recursos da Vtex.
