# Trilha de estágio

## Módulo 1

### Git

- Aprender sobre o Git Flow e como funciona a movimentação das diversas branches no dia a dia da empresa.

### Markdown

- Saber como funciona a linguagem de marcação, para a escrita de documentações diversas, exposição de códigos em plataformas, etc.
- Escrever um README no repositório pessoal do GitLab destinado ao plano de estágio.

### CSS

- Aprender as funções básicas do CSS.
- Ver como funciona o framework Bootstrap.
- Aprender as funções do CSS Grid e CSS Flex, através de exercícios práticos: GRID GARDEN e FLEX FROGGY, respectivamente.

### JavaScript

- Aprender como funciona a linguagem.
- Fazer uma calculadora usando JavaScript.
