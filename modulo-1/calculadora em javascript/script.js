// Entered numbers
function insertNumber(num) {
  document.getElementById('screen').innerHTML += num
}

//Entered operators
function insertOperator(oper) {
  document.getElementById('screen').innerHTML += oper
}

//Clean the screen
function clean() {
  document.getElementById('screen').innerHTML = ''
}

//Results on the screen
function calculate() {
  const result = document.getElementById('screen').innerHTML
  if (result) {
    document.getElementById('screen').innerHTML = eval(result)
  }
}
